variable "region" {
  description = "The AWS region to deploy in"
  type        = string
  default     = "ap-northeast-1"
}

variable "ami" {
  description = "The AMI to use for the instance"
  type        = string
  default     = "ami-0b9a26d37416470d2"
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t2.micro"
}

