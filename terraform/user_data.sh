#!/bin/bash
sudo yum update -y
sudo yum install -y docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
sudo yum install git -y
git --version 
git clone https://gitlab.com/gearsinsky/cicd_pipline.git
#git clone https://gearsinsky:token@gitlab.com/gearsinsky/cicd_pipline.git
cd cicd_pipline
$(aws ecr get-login-password --region ap-northeast-1 | docker login --username AWS --password-stdin 602226777984.dkr.ecr.ap-northeast-1.amazonaws.com)
docker-compose down
export COMPOSE_PROJECT_NAME=app-server
docker-compose up -d




