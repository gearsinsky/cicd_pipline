output "app_server_blue_id" {
  description = "The ID of the EC2 instance"
  value       = aws_instance.app-server-blue.id
}

output "app_server_green_id" {
  description = "The ID of the EC2 instance"
  value       = aws_instance.app-server-green.id
}
output "elb_dns_name" {
  description = "The DNS name of the ELB"
  value       = aws_lb.app-server.dns_name
}

output "elb_arn" {
  description = "The ARN of the ELB"
  value       = aws_lb.app-server.arn
}

output "blue_target_group_arn" {
  description = "The ARN of the blue target group"
  value       = aws_lb_target_group.blue.arn
}

output "green_target_group_arn" {
  description = "The ARN of the green target group"
  value       = aws_lb_target_group.green.arn
}
