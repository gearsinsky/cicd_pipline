provider "aws" {
  region = var.region
}

resource "aws_vpc" "pipline" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "pipline"
  }
}

resource "aws_internet_gateway" "piplineIGW" {
  vpc_id = aws_vpc.pipline.id

  tags = {
    Name = "piplineigwIGW"
  }
}

resource "aws_route_table" "piplineRT" {
  vpc_id = aws_vpc.pipline.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.piplineIGW.id
  }

  tags = {
    Name = "pipline-RT"
  }
}

resource "aws_subnet" "for_pipline_1a" {
  vpc_id            = aws_vpc.pipline.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ap-northeast-1a"

  tags = {
    Name = "for_pipline-subnet_1a"
  }
}

resource "aws_subnet" "for_pipline_1c" {
  vpc_id            = aws_vpc.pipline.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "ap-northeast-1c"

  tags = {
    Name = "for_pipline-subnet_1c"
  }
}

resource "aws_route_table_association" "subnet_table_1a" {
  subnet_id      = aws_subnet.for_pipline_1a.id
  route_table_id = aws_route_table.piplineRT.id
}

resource "aws_route_table_association" "subnet_table_1c" {
  subnet_id      = aws_subnet.for_pipline_1c.id
  route_table_id = aws_route_table.piplineRT.id
}

resource "aws_security_group" "pipline" {
  vpc_id = aws_vpc.pipline.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pipline-security-group"
  }
}

resource "aws_iam_role" "ec2_role" {
  name = "ec2-ecr-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ec2_ecr_policy" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2-ecr-instance-profile"
  role = aws_iam_role.ec2_role.name
}

resource "aws_instance" "app-server-blue" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.for_pipline_1a.id
  vpc_security_group_ids = [aws_security_group.pipline.id]

  iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name

  user_data = file("user_data.sh")

  tags = {
    Name = "app-server-blue"
  }
  associate_public_ip_address = true
}

resource "aws_instance" "app-server-green" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.for_pipline_1c.id
  vpc_security_group_ids = [aws_security_group.pipline.id]

  iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name

  user_data = file("user_data.sh")

  tags = {
    Name = "app-server-green"
  }
  associate_public_ip_address = true
}

resource "aws_lb" "app-server" {
  name               = "blue-green-elb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.pipline.id]
  subnets = [
    aws_subnet.for_pipline_1a.id,
    aws_subnet.for_pipline_1c.id,
  ]
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "blue" {
  name     = "blue-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.pipline.id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    path                = "/"
    matcher             = "200-299"
  }
}

resource "aws_lb_target_group" "green" {
  name     = "green-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.pipline.id

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    interval            = 30
    path                = "/"
    matcher             = "200-299"
  }
}

resource "aws_lb_target_group_attachment" "blue_attachment" {
  target_group_arn = aws_lb_target_group.blue.arn
  target_id        = aws_instance.app-server-blue.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "green_attachment" {
  target_group_arn = aws_lb_target_group.green.arn
  target_id        = aws_instance.app-server-green.id
  port             = 80
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.app-server.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"

    forward {
      target_group {
        arn    = aws_lb_target_group.blue.arn
        weight = 70
      }

      target_group {
        arn    = aws_lb_target_group.green.arn
        weight = 30
      }
    }
  }
}

