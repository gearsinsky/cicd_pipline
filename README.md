﻿1. 資料庫分離，導致部署沒意義，應該設定同一個資料庫，S3或RDS，放在私網段，做好隱秘性，禁止外網連入，也比較好做管理

1. integration\_test所需要的docker compose.yml檔，目前是使用git clone，可放在S3或是個別抓取，比較不會佔空間，剛好runner是ec2，賦予權限可以抓，同個生態系可以少掉一些失誤

1. 藍綠部署可在進階成金絲雀部署

1. .terraform可用module和variables做使用，較有結構性和整齊，也可以將現有資源透過terraform導入，完成不停機部署

1. main分支沒有那麼容易合併，是否在develop或是QA分支多一點項目，最終要production\_deploy在合併為main

1. 上網研究大家的pipline是如何規劃的，因應每家公司不同做法

